
/**
 * Write a description of class Juguera here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Juguera extends Producto
{
    // instance variables - replace the example below with your own
    private int numeroDeVelocidades;
    private String materialFrasco;
    
    /**
     * Constructor for objects of class Juguera
     */
    public Juguera()
    {
        // initialise instance variables
        super();
        numeroDeVelocidades = 0;
        materialFrasco= "plastico";
    }
    
    public Juguera(int numeroDeVelocidades, String materialFrasco, String codigo, String marca, String estado, int valorReparacion, int precioRepuestos )
    {
     super(codigo, marca, estado, valorReparacion, precioRepuestos);
     if(numeroDeVelocidades>=0 && numeroDeVelocidades<=10){
       this.numeroDeVelocidades= numeroDeVelocidades;
    }
    else{
        System.out.println("ingrese numero de 0 a 10");
    }
    
     this.materialFrasco= materialFrasco;
    
    
    }
    
    public int getNumeroDeVelocidades() {
        return numeroDeVelocidades;
    }

    public void setNumeroDeVelocidades(int numeroDeVelocidades) {
        if(numeroDeVelocidades>=0 && numeroDeVelocidades<=10){
       this.numeroDeVelocidades= numeroDeVelocidades;
    }
    else{
        System.out.println("ingrese numero de 0 a 10");
    }
        
    }

    public String getMaterialFrasco() {
        return materialFrasco;
    }

    public void setMaterialFrasco(String materialFrasco) {
        this.materialFrasco = materialFrasco;
    }
    
   
}
