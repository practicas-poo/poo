
/**
 * Write a description of class Producto here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Producto
{
    // instance variables - replace the example below with your own
    
    private String codigo;
    private String marca;
    private String estado;
    private double valorReparacion;
    private double precioRepuestos;
    /**
     * Constructor for objects of class Producto
     */
    public Producto()
    {
        // initialise instance variables
        codigo = "1234";
        marca = "Madensa";
        estado= "R";
        valorReparacion = 0;
        precioRepuestos=0;
        
    }
    
    public Producto(String codigo, String marca, String estado, int valorReparacion, int precioRepuestos)
    {
        this.codigo = codigo;
        if(marca == "Madensa"|| marca=="LG"|| marca == "Fensa"){
             this.marca = marca;}
        else{
             System.out.println("las marcas deben ser Madensa, LG o Fensa solamente");  
                
                }
             
        if(estado== "R"|| estado =="r"|| estado =="E"|| estado == "e"|| estado == "F"|| estado=="f"){
             this.estado = estado;
        }else{
             System.out.println("los estados son R,E o F");
        }
        this.valorReparacion= valorReparacion;
        this.precioRepuestos= precioRepuestos;
    
    
    }
    
     public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        if(marca == "Madensa"|| marca=="LG"|| marca == "Fensa"){
             this.marca = marca;}
        else{
             System.out.println("las marcas deben ser Madensa, LG o Fensa solamente");  
                
                }
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getValorReparacion() {
        return valorReparacion;
    }

    public void setValorReparacion(int valorReparacion) {
        this.valorReparacion = valorReparacion;
    }

    public double getPrecioRepuestos() {
        return precioRepuestos;
    }

    public void setPrecioRepuestos(int precioRepuestos) {
        this.precioRepuestos = precioRepuestos;
    }
    
    
    public double obtenerTotal(Reparable iva)
    {
        double subTotalReparacion=0;
        double totalReparacion=0;
        double agregaIva=0;
        agregaIva= iva.getIva();
        subTotalReparacion= (getValorReparacion()+ getPrecioRepuestos());
        totalReparacion = (subTotalReparacion + subTotalReparacion*agregaIva);
        
        return totalReparacion;
    }
    
    public double obtenerRepuesto(double cant)
    {
        double costoRepuestos=0;
        costoRepuestos= getPrecioRepuestos()*cant;
        double pagoPorRepuesto=0;
        pagoPorRepuesto= costoRepuestos/cant;
        return pagoPorRepuesto;
    
    }
  
    
    
    
    
    
    
    
    
    
    
    
}
