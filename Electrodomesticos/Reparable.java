
/**
 * Write a description of class Reparable here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Reparable
{
    // instance variables - replace the example below with your own
    private double iva;

    /**
     * Constructor for objects of class Reparable
     */
    public Reparable()
    {
        // initialise instance variables
        iva = 0.19;
    }

     public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }
    
}
