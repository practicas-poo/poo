
/**
 * Write a description of class Lavadora here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lavadora extends Producto
{
    // instance variables - replace the example below with your own
    private int kilos;

    /**
     * Constructor for objects of class Lavadora
     */
    public Lavadora()
    {
        // initialise instance variables
        super();
        kilos = 0;
    }
    
    
     public int getKilos() {
        return kilos;
    }

    public void setKilos(int kilos) {
        this.kilos = kilos;
    }
    
}
