/**
 * TicketMachine modela una maquina de venta de pasajes de tarifa plana,
 * como los que utilizaban en Santiago los buses amarillos, pero que
 * siguen siendo usados en otras ciudades, como por ejemplo, Buenos Aires.
 * 
 * El precio de un pasaje es definido a traves del constructor.
 * 
 * Las instancias de esta clase deben verificar que el usuario solo ingrese
 * cantidades validas de dinero, y que el boleto solo se va a imprimir una vez
 * que se ha ingresado suficiente dinero.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class TicketMachine
{
    // El precio de un pasaje en esta maquina.
    private int price;
    // La cantidad de dinero que se ha ingresado hasta ahora.
    private int balance;
    // El total de dinero recaudado hasta ahora por esta maquina.
    private int total;

    /**
     * Crea una maquina que emite pasajes al precio indicado.
     * Note que el precio deberia ser mayor a cero, pero no
     * hay nada que lo garantice.
     */
    public TicketMachine(int cost)
    {
        price = cost;
        balance = 0;
        total = 0;
    }

    /**
     * Retorna el precio de un pasaje.
     */
    public int getPrice()
    {
        return price;
    }

    /**
     * Retorna el monto de dinero que se ha insertado hasta ahora para pagar el 
     * proximo pasaje.
     */
    public int getBalance()
    {
        return balance;
    }


    /**
     * Recibe una cantidad de dinero del cliente.
     * Verifica que esa cantidad es valida.
     */
    public void insertMoney(int amount)
    {
        if(amount > 0) {
            balance = balance + amount;
        }
        else {
            System.out.println("Use una cantidad positiva en lugar de: " +
                               amount);
        }
    }

    /**
     * Imprime un boleto si se ha insertado suficiente dinero,
     * y reduce el balande en el valor del precio del boleto.
     * Imprime un mensaje de error si hace falta ingresar mas dinero.
     */
    public void printTicket()
    {
        int cuantoFalta = balance - price;
        
        if (cuantoFalta <= 0) {
            if(balance >= price) {
                // Simulala impresion del boleto.
                System.out.println("##################");
                System.out.println("# La linea BlueJ ");
                System.out.println("# Boleto");
                System.out.println("# " + price + " pesos.");
                System.out.println("##################");
                System.out.println();
    
                // actualiza el total recolectado con el precio.
                total = total + price;
                // Disminuye el balance, restandole el valor del precio.
                balance = balance - price;
            }else {
                System.out.println("Debes ingresar al menos : " +
                                       (price - balance) + " pesos mas.");        
            }
        }else{
            System.out.println("Debes ingresar al menos : " +
                                       cuantoFalta + " pesos mas.");
        }
        
        
    }

    /**
     * Da el vuelto, o sea, que retorna el dinero en el balance, 
     * pero ademas limpia el balance.
     */
    public int refundBalance()
    {
        int amountToRefund;
        amountToRefund = balance;
        balance = 0;
        return amountToRefund;
    }
    
    /*public int refundBalance()
    {
        return balance;
        balance = 0;
    }*/
    
    
    /**
    *retorna el total y deja el total en cero
    */
    public int emptyMachine(){
        int retorno = total;
        total = 0;
        return retorno;
    }
}
