
/**
 * Write a description of class Dispositivo here.
 * 
 * @author (jj) 
 * @version (a version number or a date)
 */
public class Dispositivo
{
    // instance variables - replace the example below with your own
    private String codigoDispositivo;
    private String fabricante;

    /**
     * Constructor for objects of class Dispositivo
     */
    public Dispositivo(String codigoDispositivo, String fabricante )
    {
        // initialise instance variables
        this.codigoDispositivo = codigoDispositivo;
        this.fabricante= fabricante;
    }
    
    public Dispositivo()
    {
    this.codigoDispositivo = "12334";
    this.fabricante= "d-link";
    
    }
    
    public boolean cumpleMinimo(DistroVersion distroVersion){
        return false;
               
    
    }
    
    public String getCodigoDispositivo() {
        return codigoDispositivo;
    }

    public void setCodigoDispositivo(String codigoDispositivo) {
        this.codigoDispositivo = codigoDispositivo;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    
    
}
