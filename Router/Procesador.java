
/**
 * Write a description of class Procesador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Procesador extends Dispositivo
{
    // instance variables - replace the example below with your own
    private String tipo;
    private int velocidadReloj;

    /**
     * Constructor for objects of class Procesador
     */
    public Procesador()
    {
        super();
        tipo = "Broadcom BCM5354 a 240 MHZ";
        velocidadReloj = 0;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getVelocidadReloj() {
        return velocidadReloj;
    }

    public void setVelocidadReloj(int velocidadReloj) {
        this.velocidadReloj = velocidadReloj;
    }
    
}
