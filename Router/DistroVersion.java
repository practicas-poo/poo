
/**
 * Write a description of class DistroVersion here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DistroVersion
{
    // instance variables - replace the example below with your own
    private String version;
    private String nombre;
    private int memoriaRamMinima;
    private int memoriaFlashMinima;
    private int velocidadRelojMinima;

    /**
     * Constructor for objects of class DistroVersion
     */
    public DistroVersion()
    {
        // initialise instance variables
        version = "2.0";
        nombre = "rt-jj";
        memoriaRamMinima= 0;
        memoriaFlashMinima=0;
        velocidadRelojMinima=0;
        
        
        
        
    }

     public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMemoriaRamMinima() {
        return memoriaRamMinima;
    }

    public void setMemoriaRamMinima(int memoriaRamMinima) {
        this.memoriaRamMinima = memoriaRamMinima;
    }

    public int getMemoriaFlashMinima() {
        return memoriaFlashMinima;
    }

    public void setMemoriaFlashMinima(int memoriaFlashMinima) {
        this.memoriaFlashMinima = memoriaFlashMinima;
    }

    public int getVelocidadRelojMinima() {
        return velocidadRelojMinima;
    }

    public void setVelocidadRelojMinima(int velocidadRelojMinima) {
        this.velocidadRelojMinima = velocidadRelojMinima;
    }
}
