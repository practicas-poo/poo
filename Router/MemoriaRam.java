
/**
 * Write a description of class MemoriaRam here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MemoriaRam extends Dispositivo
{
    // instance variables - replace the example below with your own
    private String tipo;
    private int cantidad;
    private boolean ampliable;

    /**
     * Constructor for objects of class MemoriaRam
     */
    public MemoriaRam()
    {
        // initialise instance variables
        tipo = "Samsung K4S641632K-UC75 ";
        cantidad = 8;
        ampliable = true;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public boolean getAmpliable() {
        return ampliable;
    }

    public void setAmpliable(boolean ampliable) {
        this.ampliable = ampliable;
    }
}
