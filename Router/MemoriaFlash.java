
/**
 * Write a description of class MemoriaFlash here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MemoriaFlash extends Dispositivo
{
    // instance variables - replace the example below with your own
    private String tipo;
    private int cantidad;

    /**
     * Constructor for objects of class MemoriaFlash
     */
    public MemoriaFlash()
    {
        // initialise instance variables
        tipo = "Samsung K801716UBC PI07";
        cantidad =2;
    }

    
     public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
   
}
