
public class Main
{
    public static void main(String[] args)
    {
        
        Cliente cliente1 = new Cliente("21623469-3", "NN", 'N');
        Tarjeta tarjeta1 = new Tarjeta(cliente1, 11111111, "01/03/2014", 15000, true, 20);
        
        tarjeta1.setSaldo(10000);
        System.out.println("Nuevo saldo: " + tarjeta1.getSaldo());
        
        tarjeta1.setNumeroTarjeta(111126);
        tarjeta1.validarTarjetaEstudiante("111111111-1", 111111111);
        System.out.println("Tipo cliente: "+cliente1.getTipo());
    }
}
