
public class Cliente
{
    private String rut;
    private String nombre;
    private char tipo;
    
    public Cliente(String rut, String nombre, char tipo)
    {
        setRut(rut);
        setNombre(nombre);
        setTipo(tipo);
    }
    
    public void imprimir()
    {
        System.out.println("RUT Cliente: " + getRut());
        System.out.println("Nombre Cliente: "+getNombre());
        System.out.println("Tipo Cliente: "+getTipo());
    }

    public String getRut() {
		return rut;
	}
	
	public void setRut(String rut) {
		if(rut.trim().length() >= 8){
		    this.rut = rut;
		}else{
		    System.out.println("El RUT debe contener 8 caracteres como mínimo.");
		}
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		if(nombre.trim().length() >= 3){
		    this.nombre = nombre;
		}else{
		    System.out.println("Ingrese un nombre por favor.");
		}
	}
	
	public char getTipo() {
		return tipo;
	}
	
	public void setTipo(char tipo) {
		if((tipo == 'N') || (tipo == 'E') || (tipo == 'T')){
		    this.tipo = tipo;
		}else{
		    System.out.println("Tipo cliente debe ser N(normal), E(estudiante) o T(tercera edad)");
		}
	}
}
