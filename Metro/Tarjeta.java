
public class Tarjeta
{
    private int numeroTarjeta;
    private String fechaEmision;
    private int saldo;
    private boolean activa;
    private int porcentajeDescuento;
    private Cliente cliente;
    
    public Tarjeta(Cliente cliente, int numeroTarjeta, String fechaEmision, int saldo, boolean activa,
                   int porcentajeDescuento)
    {
        setCliente(cliente);
        setNumeroTarjeta(numeroTarjeta);
        setFechaEmision(fechaEmision);
        setSaldo(saldo);
        setActiva(activa);
        setPorcentajeDescuento(porcentajeDescuento);
    }
    
    public String obtenerDatos()
    {
        String cliente = getCliente().getNombre();
        int saldo = getSaldo();
        int porcentaje = getPorcentajeDescuento();
        
        return cliente + " $" + saldo + " " + porcentaje + "%";
    }
    
    public void otorgarDescuento()
    {
        if((getActiva() == true) && (getSaldo() > 20000)){
            setPorcentajeDescuento(10);
        }else{
            System.out.println("Tu saldo no supera los $20.000 para aplicar el descuento.");
        }
    }
    
    public void descontarSaldo()
    {
        int nuevoSaldo;
        char tipoCliente = this.cliente.getTipo();
        
        if(getSaldo() >= 500){
            if((tipoCliente == 'N') || (tipoCliente == 'n')){
                nuevoSaldo = getSaldo() - (500 * (getPorcentajeDescuento() / 100));
                setSaldo(nuevoSaldo);
            }
            
            if((tipoCliente == 'E') || (tipoCliente == 'e')){
                nuevoSaldo = getSaldo() - (200 * (getPorcentajeDescuento() / 100));
                setSaldo(nuevoSaldo);
            }
            
            if((tipoCliente == 'T') || (tipoCliente == 't')){
                nuevoSaldo = getSaldo() - (300 * (getPorcentajeDescuento() / 100));
                setSaldo(nuevoSaldo);
            }
        }else{
            System.out.println("Saldo Insuficiente. RECARGA!!!!");
        }
    }
    
    public boolean validarTarjetaEstudiante(String nRut, int numeroTarjeta)
    {
        int tarjeta = numeroTarjeta;
        String rut = nRut;
        
        if((tarjeta == getNumeroTarjeta()) && (rut == getCliente().getRut())){
            getCliente().setTipo('E');
            return true;
        }else{
            return false;
        }
    }
    

    public void imprimir()
    {
        System.out.println("RUT Cliente: " + getCliente().getRut());
        System.out.println("Nombre Cliente: " + getCliente().getNombre());
        System.out.println("Tipo Cliente: "+ getCliente().getTipo());
        System.out.println("Numero Tarjeta: " + getNumeroTarjeta());
        System.out.println("Fecha de Emisión: " + getFechaEmision());
        System.out.println("Tarjeta Activa: " + getActiva());
        System.out.println("Saldo: " + getSaldo());
    }
    
   
    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }
    
    public void setNumeroTarjeta(int numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }
    
    public String getFechaEmision() {
        return fechaEmision;
    }
    
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
    public int getSaldo() {
        return saldo;
    }
    
    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    
    public boolean getActiva() {
        return activa;
    }
    
    public void setActiva(boolean activa) {
        this.activa = activa;
    }
    
    public int getPorcentajeDescuento() {
        return porcentajeDescuento;
    }
        
    public void setPorcentajeDescuento(int porcentajeDescuento) {
        if((porcentajeDescuento >= 0) && (porcentajeDescuento <= 100)){
            this.porcentajeDescuento = porcentajeDescuento;
        }else{
            System.out.println("El porcentaje de descuento debe estar entre 0 y 100.");
        }
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
