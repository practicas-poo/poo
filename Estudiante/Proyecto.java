
/**
 * Write a description of class Proyecto here.
 * 
 * @author (Francisco Mora) 
 */
public class Proyecto
{
    private String titulo;
    private Estudiante estudiante;
    
    public Proyecto(String nuevoTitulo, Estudiante nuevoEstudiante)
    {
        this.titulo = nuevoTitulo;
        this.estudiante = nuevoEstudiante;
    }
    
    public void setTitulo(String nuevoTitulo)
    {
        this.titulo = nuevoTitulo;
    }
    
    public String getTitulo()
    {
        return this.titulo;
    }
    
    public void setEstudiante(Estudiante nuevoEstudiante)
    {
        this.estudiante = nuevoEstudiante;
    }
    
    public Estudiante getEstudiante()
    {
        return this.estudiante;
    }
    
    public void imprimirFicha()
    {
        System.out.println("Nombre del Estudiante: "+ this.estudiante.getNombre());
        System.out.println("Rut: "+ this.estudiante.getRut());
        System.out.println("Carrera: "+ this.estudiante.getCarrera());
        System.out.println("Titulo Proyecto: "+ getTitulo());
    }
}
