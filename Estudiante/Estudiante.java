
/**
 * Write a description of class Estudiante here.
 * 
 * @author (Francisco Mora) 
 */
public class Estudiante
{
    //declaro variables
    private String nombre;
    private String rut;
    private String carrera;
    
    public Estudiante(String nombreEstudiante, String rutEstudiante, String nombreCarrera)
    {
        this.nombre = nombreEstudiante;
        this.rut = rutEstudiante;
        this.carrera = nombreCarrera;
    }
    
    public void setNombre(String nuevoNombre)
    {
        this.nombre = nuevoNombre;
    }
    
    public String getNombre()
    {
        return this.nombre;
    }
    
    public void setRut(String nuevoRut)
    {
        this.rut = nuevoRut;
    }
    
    public String getRut()
    {
        return this.rut;
    }
    
    public void setCarrera(String nuevoCarrera)
    {
        this.carrera = nuevoCarrera;
    }
    
    public String getCarrera()
    {
        return this.carrera;
    }
}
