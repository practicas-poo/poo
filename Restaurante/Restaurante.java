
/**
 * Clase Restaurante
 * @author (Francisco Mora) 
 * @version (07-06-2014)
 */
public class Restaurante
{
    //variables
    private String nombreRestaurante;
    private String direccion;
    private int ventasDiarias;
    private int capacidadTotal;
    private int numeroMozos;
    
    //constructor con parametros
    public Restaurante(String nombreRestaurante, String direccion, 
                       int ventasDiarias, int capacidadTotal, int numeroMozos)
    {
        this.nombreRestaurante = nombreRestaurante;
        this.direccion = direccion;
        this.ventasDiarias = ventasDiarias;
        this.capacidadTotal = capacidadTotal;
        this.numeroMozos = numeroMozos;
    }
    
    //getter y setters
    public String getNombreRestaurante()
    {
        return this.nombreRestaurante;
    }
    
    public void setNombreRestaurante(String nombreRestaurante)
    {
        this.nombreRestaurante = nombreRestaurante;
    }
    
    public String getDireccion()
    {
        return this.direccion;
    }
    
    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }
    
    public int getVentasDiarias()
    {
        return this.ventasDiarias;
    }
    
    public void setVentasDiarias(int ventasDiarias)
    {
        this.ventasDiarias = ventasDiarias;
    }
    
    public int getCapacidadTotal()
    {
        return this.capacidadTotal;
    }
    
    public void setCapacidadTotal(int capacidadTotal)
    {
        this.capacidadTotal = capacidadTotal;
    }
    
    public int getNumeroMozos()
    {
        return this.numeroMozos;
    }
    
    public void setNumeroMozos(int numeroMozos)
    {
        this.numeroMozos = numeroMozos;
    }
}
