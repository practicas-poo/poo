
/**
 * Clase Administrador
 * 
 * @author (Francisco Mora) 
 * @version (07-06-2014)
 */
public class Administrador
{
    //variables
    private String rut;
    private String nombre;
    private int edad;
    private int anniosPermanencia;
    private Restaurante restaurante;
    
    //constructor
    public Administrador(String rut, String nombre, int edad,
                         int anniosPermanencia, Restaurante restaurante)
    {
        this.rut = rut;
        this.nombre = nombre;
        this.edad = edad;
        this.anniosPermanencia = anniosPermanencia;
        this.restaurante = restaurante;
    }
    
    //getter and setters
    public String getRut()
    {
        return this.rut;
    }
    
    public void setRut(String rut)
    {
        this.rut = rut;
    }
    
    public String getNombre()
    {
        return this.nombre;
    }
    
    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }
    
    public int getEdad()
    {
        return this.edad;
    }
    
    public void setEdad(int edad)
    {
        this.edad = edad;
    }
    
    public int getAnniosPermanencia()
    {
        return this.anniosPermanencia;
    }
    
    public void setAnniosPermanencia(int anniosPermanencia)
    {
        this.anniosPermanencia = anniosPermanencia;
    }
    
    public Restaurante getRestaurante()
    {
        return this.restaurante;
    }
    
    public void setRestaurante(Restaurante restaurante)
    {
        this.restaurante = restaurante;
    }
}
