
/**
 * Clase Dueño
 * 
 * @author (Francisco Mora) 
 * @version (07-06-2014)
 */
public class Dueño
{
    private Restaurante restaurante;
    private Administrador administrador;
    
    public Dueño(Restaurante restaurante, Administrador administrador)
    {
        this.restaurante = restaurante;
        this.administrador = administrador;
    }
    
    //Obtengo las ventas diarias en pesos del administrador del restaurant
    public void ventasDiarias()
    {
        String nombre = administrador.getNombre();
        String rut = administrador.getRut();
        int ventas = restaurante.getVentasDiarias();
        String local = restaurante.getNombreRestaurante();
        String direccion = restaurante.getDireccion();
        
        System.out.println("==============================");
        System.out.println("Nombre Administrador: "+nombre);
        System.out.println("Nombre Restaurante a Cargo: "+local);
        System.out.println("Dirección del Restaurante: "+direccion);
        System.out.println("RUT Administrador: "+rut);
        System.out.println("El total de ventas diarias es: "+ventas);
        System.out.println("==============================");
    }
    
    //Obtengo el promedio de consumo por persona, de acuerdo
    //a la capacidad total del local.
    public void consumoPorPersona()
    {
        int ventas = restaurante.getVentasDiarias();
        int capacidad = restaurante.getCapacidadTotal();
        String local = restaurante.getNombreRestaurante();
        
        int promedio = ventas / capacidad;
        
        System.out.println("==============================");
        System.out.println("Nombre del Local: "+local);
        System.out.println("El consumo promedio por persona es: "+ promedio);
        System.out.println("==============================");
    }
    
    //Obtengo el promedio de ventas por mozo de cada Restaurante.
    public void promedioVentasPorMozo()
    {
        int ventas = restaurante.getVentasDiarias();
        int mozos = restaurante.getNumeroMozos();
        String local = restaurante.getNombreRestaurante();
        
        int promedio = ventas / mozos;
        
        System.out.println("==============================");
        System.out.println("Nombre del Local: "+local);
        System.out.println("El promedio de ventas por mozo es: "+ promedio);
        System.out.println("==============================");
    }
}
