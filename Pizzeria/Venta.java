

/**
 * Class: Venta
 * Name: Gustavo Naoto Aguilar Morita
 * Date: June 13, 2014
 */
public class Venta
{
    private Pizza pizza;
    private Bebida bebida;
    private int total;
   
    public Venta(Pizza pizza, Bebida bebida)
    {
        this.pizza = pizza;
        this.bebida = bebida;
    }
    
    public void mostrarCompra()
    {
        pizza.imprimirPizza();
        bebida.imprimirBebida();
    }
    
    public int mostrarPrecio()
    {
        total = pizza.getPrecio() + bebida.getPrecio();
        return total;
    }
    
    public boolean pagar(int pago)
    {
        if(pago>=total)
        {
            System.out.println("Su compra es exitosa, su vuelto es: "+(pago-total));
            return true;
        }
        else
        {
            System.out.println("No se ha realizado la compra, falta: "+(total-pago));
            return false;
        }
    }
    
    public int aplicarDescuento(boolean descuento)
    {
        if(descuento == true)
        {
            total = total - 2500;
            System.out.println("Descuento realizado");
            return total;
        }
        else
        {
            System.out.println("Suscríbase para que obtenga un descuento de $2.500!");
            return total;
        }
        
    }
    
    public void modificarPizza(String tamaño)
    {
        pizza.setTamaño(tamaño);
    }
    
    public void modificarBebida(String sabor)
    {
        bebida.setSabor(sabor);
    }

}



