
/**
 * Class: Pizza
 * Name: Gustavo Naoto Aguilar Morita
 * Date: June 11, 2014
 */
public class Pizza
{
    // Definimos los atributos
    private String tamaño;
    private String ingredientes;
    private String masa;
    private int precio;
    private int cIngredientes;
    
    // Constructor con valores por defecto.
    //Pizza Vegetariana. En este caso no recibe parámetros, sólo definimos.
    public Pizza()
    {
        this.tamaño = "Mediana";
        this.ingredientes = "Choclo, Espárragos, Champiñones y Cebolla";
        this.masa = "Tradicinal";
    }
    
    // Desafío
   /**
    * public Pizza(int opcion, String tamaño)
    * {
    *     //Pizza Vegetariana
    *     if(opcion == 1)
    *     {
    *        this.tamaño = tamaño
    *        this.ingredientes = "Choclo, Espárragos, Champiñones y Cebolla";
    *        this.masa = "Tradicinal";
    *        this.precio = 10990; 
    *     }
    *     // Pizza Tejana
    *     if(opcion == 2)
    *     {
    *         this.tamaño = tamaño";
    *         this.ingredientes = "Tocino, Carne, Cebolla y Pimentones. Base: Salsa BBQ";
    *         this.masa = "Tradicinal";
    *         this.precio = 13990;
    *     }
    *     ....etc....
    * }
    *     
    */
   /**
    * Pizzas Personalizadas
    */
    public Pizza(String tamaño, int cIngrediente, String ingredientes, String masa)
    {
        this.tamaño = tamaño;
        this.ingredientes = ingredientes;
        this.masa = masa;
        this.cIngredientes = cIngredientes;
        
    }
   
    // Calculo del precio
   public void precioArmada()
   {
       if(tamaño == "Chica" || tamaño == "chica")
       {
           precio = 6990;
       }
       if(tamaño == "Mediana" || tamaño == "mediana")
       {
           precio = 10990;
       }
       if(tamaño == "Familiar" || tamaño == "familiar")
       {
           precio = 13990;
       }
   }
     public void precioPersonalizada()
   {
       if(tamaño == "Chica" || tamaño == "chica")
       {
           precio = 3990;
       }
       if(tamaño == "Mediana" || tamaño == "mediana")
       {
           precio = 7990;
       }
       if(tamaño == "Familiar" || tamaño == "familiar")
       {
           precio = 10990;
       }
       // Precio base + el precio por cada ingrediente pedido.
       precio = precio + cIngredientes*1000;
   }
    
   public int getPrecio()
   {
        return precio;
   }
   
  // Métodos para setear las características de las pizzas.
   public void setIngredientes(String ingredientes)
   {
       this.ingredientes = ingredientes;
   }
   
   public void setMasa(String masa)
   {
       this.masa = masa;
   }
   
   public void setTamaño(String tamaño)
   {
       this.tamaño = tamaño;
   }
   
   public void imprimirPizza()
   {
       System.out.println("Pizza: ");
       System.out.println("Tamaño: "+tamaño);
       System.out.println("Masa: "+masa);
       System.out.println("Ingredientes: "+ingredientes);
       System.out.println("Precio: "+precio);
   }

}

