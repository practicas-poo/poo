/**
 * Class: Bebida
 * Name: Gustavo Naoto Aguilar Morita
 * Date: June 11, 2014
 */
public class Bebida
{
    private String sabor;
    private double tamañoEnLitros;
    private int precio;
    
    public Bebida(String sabor, double tamañoEnLitros, int precio)
    {
        this.sabor = sabor;
        this.tamañoEnLitros = tamañoEnLitros;
        this.precio = precio;
    }
    
    public int getPrecio()
    {
        return precio;
    }
    public void setPrecio(int precio)
    {
        this.precio = precio;
    }
    
    public double getLitros()
    {
        return tamañoEnLitros;
    }
    public void setLitros(double tamañoEnLitros)
    {
        this.tamañoEnLitros = tamañoEnLitros;
    }
    
    public String getSabor()
    {
        return sabor;
    }
    public void setSabor(String sabor)
    {
        this.sabor = sabor;
    }
    
    public void imprimirBebida()
    {
        System.out.println("Bebida: ");
        System.out.println("Sabor: "+sabor);
        System.out.println("Tamaño: "+tamañoEnLitros+" Litros");
        System.out.println("Precio: "+precio);
    }

}
