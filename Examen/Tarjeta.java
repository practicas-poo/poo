
/**
 * Write a description of class Tarjeta here.
 * 
 * @author (Francisco Mora) 
 * @version (14/07/2014)
 */
public class Tarjeta
{
    private int codigo;
    private String fechaDeEmision;
    private int diasTranscurridos;
    private boolean tarjetaActiva;
    private int clave;
    private double porcentajeDescuento;
    private Alumno alumno;
    
    public Tarjeta(Alumno alumno, int codigo, String fechaDeEmision, int diasTranscurridos, int clave, double porcentajeDescuento)
    {
       setAlumno(alumno);
       setCodigo(codigo);
       setFechaDeEmision(fechaDeEmision);
       setDiasTranscurridos(diasTranscurridos);
       this.tarjetaActiva = false;
       setClave(clave);
       setPorcentajeDescuento(porcentajeDescuento);
    }
    
    public void activarTarjeta()
    {
        boolean alumno = getAlumno().getAlumnoRegular();
        boolean tarjetaActiva = getTarjetaActiva();
        
        if((alumno == true) && (getDiasTranscurridos() == 5))
        {
            this.tarjetaActiva = true;
            setPorcentajeDescuento(10.0);
            setClave(1234);
        }else{
            this.tarjetaActiva = false;
        }
    }
    
    public void modificarClave(int claveAntigua, int claveNueva)
    {
        if(claveAntigua == getClave())
        {
            setClave(claveNueva);
        }
    }
    
    public void aumentarPorcentajeDescuento(double nuevoPorcentaje)
    {
        setPorcentajeDescuento(nuevoPorcentaje);
    }
    
    public String imprimir()
    {               
        if(getTarjetaActiva() == true)
        {
            String tipoAlumno = "";
            String rut = getAlumno().getRut();
            int codigo = getCodigo();
            double descuento = getPorcentajeDescuento();
            
            if(getAlumno().getAlumnoRegular() == true)
            {
                tipoAlumno = "REGULAR";
            }else{
                tipoAlumno = "NO REGULAR";
            }
            
            return rut + " " + tipoAlumno + " " + clave + " " + descuento + "%";
        }else{
            return "Tarjeta del alumno inactiva";
        }
    }
    
    //get y set
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getFechaDeEmision() {
        return fechaDeEmision;
    }

    public void setFechaDeEmision(String fechaDeEmision) {
        this.fechaDeEmision = fechaDeEmision;
    }

    public int getDiasTranscurridos() {
        return diasTranscurridos;
    }

    public void setDiasTranscurridos(int diasTranscurridos) {
        if((diasTranscurridos >= 0) || (diasTranscurridos <= 5))
        {
            this.diasTranscurridos = diasTranscurridos;
        }
    }

    public boolean getTarjetaActiva() {
        return tarjetaActiva;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public double getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(double porcentajeDescuento) {
        if((porcentajeDescuento >= 0.0) || (porcentajeDescuento <= 100.0))
        {
            this.porcentajeDescuento = porcentajeDescuento;
        }
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}
