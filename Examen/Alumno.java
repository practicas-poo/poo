
/**
 * Write a description of class Alumno here.
 * 
 * @author (Francisco Mora) 
 * @version (14/07/2014)
 */
public class Alumno
{
    private String rut;
    private String nombre;
    private String apellido;
    private char sexo;
    private boolean alumnoRegular;
    
    public Alumno(String rut, String nombre, String apellido, char sexo, boolean alumnoRegular)
    {
        setRut(rut);
        setNombre(nombre);
        setApellido(apellido);
        setSexo(sexo);
        setAlumnoRegular(alumnoRegular);
    }
    
    //get y set
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        if(rut.length() >= 8)
        {
            this.rut = rut;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        if((sexo == 'F') || (sexo == 'M'))
        {
            this.sexo = sexo;
        }
    }

    public boolean getAlumnoRegular() {
        return alumnoRegular;
    }

    public void setAlumnoRegular(boolean alumnoRegular) {
        this.alumnoRegular = alumnoRegular;
    }
}
