
/**
 * Write a description of class Prueba here.
 * 
 * @author (Francisco Mora) 
 * @version (14/07/2014)
 */
public class Prueba
{
    
    public static void main(String[] args)
    {
        Alumno alumno1 = new Alumno("16713458-9", "Pepito", "Alarcón", 'M', false);
        Alumno alumno2 = new Alumno("15345654-1", "Irene", "Moya", 'F', false);
        
        Tarjeta tarjeta1 = new Tarjeta(alumno1, 1254, "09/07/2014", 5, 1111, 10.0);
        tarjeta1.modificarClave(1111, 5555);
        tarjeta1.aumentarPorcentajeDescuento(15.0);
    }
}
