
public class Fecha
{
    //Declaro parametros
    private int dia = 01;
    private int mes = 01;
    private int agnio = 1900;
    
    //Constructor por defecto
    public Fecha()
    {
        dia   = 05;
        mes   = 04;
        agnio = 2014;
    }
    
    //Constructor con parametros
    public Fecha(int nDia, int nMes, int nAgnio)
    {               
        dia   = nDia;
        mes   = nMes;
        agnio = nAgnio;
        
        if(validarFecha()){
            if((getMes() == 2) && bisiesto() && (getDia() > 29)){
                System.out.println("el día es inválido para el mes y año..");
            }
            
            if((getMes() == 4 || getMes() == 6 || getMes() == 9 || getMes() == 11) && (getMes() > 30)){
                System.out.println("El mes tiene 30 días..");
            }
        }else{
            System.out.println("La Fecha es Inválida..");
        }
    }
    
    //imprime fecha local
    public void imprimirFechaLocal(){
        System.out.println("La fecha es: "+
                            dia+"/"+mes+"/"+agnio);
    }
    
    //imprime fecha con mes en letras
    public void imprimirFechaLetras(){
        System.out.println("La fecha es: "+dia+
                           " de "+mesLetras()+" del "+agnio);
    }
    
    //imprime la fecha con formato americano
    public void imprimirFechaAmericana(){
        System.out.println("La fecha es: "+
                            mes+"/"+dia+"/"+agnio);
    }
    
    //Transforma el numero del mes a letras
    public String mesLetras(){
        String mesLetra = "";
        
        if(mes == 1){
            mesLetra = "Enero";
        }
        
        if(mes == 2){
            mesLetra = "Febrero";
        }
        
        if(getMes() == 3){
            mesLetra = "Marzo";
        }
        
        if(mes == 4){
            mesLetra = "Abril";
        }
        
        if(mes == 5){
            mesLetra = "Mayo";
        }
        
        if(mes == 6){
            mesLetra = "Junio";
        }
        
        if(mes == 7){
            mesLetra = "julio";
        }
        
        if(mes == 8){
            mesLetra = "Agosto";
        }
        
        if(mes == 9){
            mesLetra = "Septiembre";
        }
        
        if(mes == 10){
            mesLetra = "Octubre";
        }
        
        if(mes == 11){
            mesLetra = "Noviembre";
        }
        
        if(mes == 12){
            mesLetra = "Diciembre";
        }
        
        return mesLetra;
    }
    
    //válida fechas permitidas
    public boolean validarFecha(){
        if((getDia() >= 1 && getDia() <= 31) && (getMes() >= 1 && getMes() <= 12) && (getAgnio() >= 1900)){
            return true;
        }else {
            return false;
        }
    }
    
    //valida si un año es bisiesto.
    public boolean bisiesto(){
        if((getAgnio() % 4 == 0) && ((getAgnio() % 100 != 0) || (getAgnio() % 400 == 0))){
            return true;
        }else{
            return false;
        }
    }
    
    
    //getters y setters
    public int getDia(){
        return dia;
    }
    
    public void setDia(int nuevoDia){
        if((nuevoDia >= 1) && (nuevoDia <= 31)){
            dia = nuevoDia;
        }else{
            System.out.println("Un mes sólo tiene hasta 31 dias..");
        }
    }
    
    public int getMes(){
        return mes;
    }
    
    public void setMes(int nuevoMes){
        if((nuevoMes >= 1) && (nuevoMes <= 12)){
            mes = nuevoMes;
        }else{
            System.out.println("Mes Inválido");
        }
    }
    
    public int getAgnio(){
        return agnio;
    }
    
    public void setAgnio(int nuevoAgnio){
        if(nuevoAgnio > 1900){
            agnio = nuevoAgnio;
        }else{
            System.out.println("Ingrese un año superior o igual a 1900");
        }
    }
}
